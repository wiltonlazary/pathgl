pathgl
======

draw svg paths using webgl


### TODO ### 

* Make parser comply with spec

* stroke
  * thickness
  * dasharray
  * length
  * opacity
  * color

* fill
  * pattern
  * gradient + shaders
  * color
  * opacity
